# Indicate where to source the terraform module from.
# The URL used here is a shorthand for
# "tfr://registry.terraform.io/terraform-aws-modules/vpc/aws?version=3.5.0".
# Note the extra `/` after the protocol is required for the shorthand
# notation.
generate "backend" {
  path      = "backend.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  backend "http" {
  }
}
EOF
}

# Indicate the input values to use for the variables of the module.
inputs = {
  name = "terraform-noop"
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
